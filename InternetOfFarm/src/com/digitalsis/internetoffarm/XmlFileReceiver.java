package com.digitalsis.internetoffarm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class XmlFileReceiver extends BroadcastReceiver{
	
	private static final String TAG = "XmlFileManager";
	private static XmlFileReceiver xmlFM = null;
	private static File xmlFilePath = null;
	private static File xmlFile = null;
	
	public static XmlFileReceiver getInstance() {
		if(xmlFM == null) {
			xmlFM = new XmlFileReceiver();
		}
		return xmlFM;
	}
	
	public File getFile(){
		if(xmlFilePath == null) {
			String dirPath = "storage/sdcard0/iof";
			xmlFilePath = new File(dirPath);
			if(!xmlFilePath.exists()) {
				xmlFilePath.mkdir();
			}
		}
		if(xmlFile == null) {
			xmlFile = new File(xmlFilePath.getPath()+"/allCollect.xml");
		}
		return xmlFile;
	}
	
	public StringBuilder sortXml(StringBuilder revMsg){
		Log.d(TAG, "jun02v sortXml");
		StringBuilder xmlStrBuff = new StringBuilder(revMsg);
		int tabNum = 1;
		for(int i=0 ; i<xmlStrBuff.length() ; i++) {
			char sortChar = xmlStrBuff.charAt(i); 
			if(sortChar=='\n' || sortChar=='\t') {
				xmlStrBuff.deleteCharAt(i);
				i--;
			}
			/*if(sortChar == '>') {
				Log.d(TAG, "jun02v sortChar:"+sortChar+", next:"+xmlStrBuff.charAt(i+1));
				if(xmlStrBuff.charAt(i+1) == '<') {
					xmlStrBuff.insert(i+1, "\n");
					if(xmlStrBuff.charAt(i+3) == '/') {
						tabNum--;
					}
					for(int j=0 ; j<=tabNum ; j++){
						xmlStrBuff.insert(i+2, "\t");
					}
					tabNum++;
				}
			}
			if(xmlStrBuff.charAt(i) == '/') {
				tabNum--;
			}*/
		}
		return xmlStrBuff;
	}
	
	public StringBuilder deletePrefix(StringBuilder revMsg) {
		int i = 0;
		StringBuilder xmlData = new StringBuilder();
		xmlData = revMsg;
		while(xmlData.charAt(i) != '<') {
			Log.d(TAG, "xmlData.charAt("+i+"):"+xmlData.charAt(i));
			xmlData = xmlData.deleteCharAt(i);
		}
		return xmlData;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		String name = intent.getAction();
		BufferedWriter bufferWriter = null;
		Log.d(TAG, "receive broadcast~!");
		if(name.equals("com.digitalsis.iof.filechanged")) {
			try {
				FileInputStream fis = new FileInputStream(xmlFile);
				BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
				StringBuilder sb = new StringBuilder();
				String line;
				while((line = reader.readLine()) != null) {
				}
				Log.d(TAG, "read xml file");
				sb = sortXml(sb);
				
				int lastIndex = sb.lastIndexOf("/"+IOFData.ALL_COL);
				lastIndex += 12;
				Log.d(TAG, "sb.charAt(lastIndex):"+sb.charAt(lastIndex)+", lastIndex:"+lastIndex+", sb.length():"+sb.length());
				if(sb.length() >= lastIndex) {
					sb.delete(lastIndex, sb.length());
				}
				
				/*FileWriter fileWriter = new FileWriter(xmlFile);
				bufferWriter = new BufferedWriter(fileWriter);
				bufferWriter.write(sb.toString());
				bufferWriter.flush();*/
				Log.d(TAG, "write xml file");
				reader.close();
				sb = deletePrefix(sb);
				xmlParser(sb.toString());				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public void xmlParser(String data){
		try {
			Log.d(TAG, "jun02v : enter xmlParser");
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = factory.newPullParser();
			String tagName = null;
			String tabValue = null;
			data = data.trim().replace("\n", "").replace("\r", "").replace("/t", "");
			parser.setInput(new StringReader(data));
			int eventType = parser.getEventType();
			boolean indoor = false;
			boolean temp = false;
			boolean outdoor = false;
			while (eventType != XmlPullParser.END_DOCUMENT) {	
				switch(eventType) {
				case XmlPullParser.START_TAG :
					tagName = parser.getName();
					//Log.d(TAG, "parser START_TAG : "+tagName);
					if (tagName.equals(IOFData.INDOOR_ENV)) {
						indoor = true;
					}
					if (tagName.equals(IOFData.TEMP)) {
						temp = true;
					}
					if (tagName.equals(IOFData.OUTDOOR_ENV)) {
						outdoor = true;
					}
					break;
				case XmlPullParser.END_TAG :
					tagName = parser.getName();
					//Log.d(TAG, "parser END_TAG : "+tagName);
					if (tagName.equals(IOFData.INDOOR_ENV)) {
						indoor = false;
						Log.d(TAG, "indoor false.");
					}
					if (tagName.equals(IOFData.TEMP)) {
						temp = false;
					}
					if (tagName.equals(IOFData.OUTDOOR_ENV)) {
						outdoor = false;
						Log.d(TAG, "outdoor false.");
					}
					break;
				case XmlPullParser.TEXT :
					tabValue = parser.getText();				
					//Log.d(TAG, "parser TEXT : "+tabValue);
					if (tagName.equals(IOFData.FARM_NAME)) {
						IOFData.setFarmName(tabValue);
					}
					if (tagName.equals(IOFData.FARM_ADDR)) {
						IOFData.setFarmAddr(tabValue);
					}
					if (tagName.equals(IOFData.CROP_ID)) {
						IOFData.setCropId(tabValue);
					}
					if (indoor) {
						if(tagName.equals(IOFData.TEMP)){
							IOFData.setTempIn(tabValue);
						} else if(tagName.equals(IOFData.HUMIDITY)) {
							IOFData.setHumidityIn(tabValue);
						} else if(tagName.equals(IOFData.PPFD)) {
							IOFData.setPpfdIn(tabValue);
						} else if(tagName.equals(IOFData.WATER_LEV)) {
							IOFData.setWaterLevelIn(tabValue);
						} else if(tagName.equals(IOFData.FLOOD)) {
							IOFData.setFloodIn(tabValue);
						} else if(tagName.equals(IOFData.DO)) {
							IOFData.setInDOIn(tabValue);
						} else if(tagName.equals(IOFData.PH)) {
							IOFData.setPhIn(tabValue);
						} else if(tagName.equals(IOFData.WATER_TEMP)) {
							IOFData.setWaterTempIn(tabValue);
						} else if(tagName.equals(IOFData.EC)) {
							IOFData.setEcIn(tabValue);
						} else if(tagName.equals(IOFData.ELEC_SENSOR)) {
							IOFData.setElectricSensorIn(tabValue);
						} else if(tagName.equals(IOFData.ELEC_STATUS)) {
							IOFData.setElectricStatusIn(tabValue);
						} else if(tagName.equals(IOFData.CO2)) {
							IOFData.setCo2In(tabValue);
						}
					}
					if (tagName.equals(IOFData.BLIND_CONTROL)) {
						IOFData.setBlindControl(tabValue);
					}
					if (tagName.equals(IOFData.CO2_CONTROL)) {
						IOFData.setCo2Control(tabValue);
					}
					if (tagName.equals(IOFData.FAN_CONTROL)) {
						IOFData.setFanControl(tabValue);
					}
					if (tagName.equals(IOFData.HEATER_CONTROL)) {
						IOFData.setHeaterControl(tabValue);
					}
					if (tagName.equals(IOFData.LED_CONTROL)) {
						IOFData.setLedControl(tabValue);
					}
					break;
				}
				eventType = parser.next();
			}
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
