package com.digitalsis.internetoffarm;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SlidingMenuAdapter extends ArrayAdapter<SlidingMenuItem>{
	private final Context context;
	private final ArrayList<SlidingMenuItem> slidingArrayList;
	
	public SlidingMenuAdapter(Context context, ArrayList<SlidingMenuItem> slidingArrayList) {
		super(context, R.layout.target_item, slidingArrayList);
		
		this.context = context;
		this.slidingArrayList = slidingArrayList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = null;
		if(!slidingArrayList.get(position).isGrouperHeader()) {
			rowView = inflater.inflate(R.layout.target_item, parent, false);
			
			ImageView imgView = (ImageView) rowView.findViewById(R.id.item_icon);
			TextView titleView = (TextView) rowView.findViewById(R.id.item_title);
			
			imgView.setImageResource(slidingArrayList.get(position).getIcon());
			titleView.setText(slidingArrayList.get(position).getTitle());
		} else {
			rowView = inflater.inflate(R.layout.group_header_item, parent, false);
			TextView titleView = (TextView) rowView.findViewById(R.id.header);
			titleView.setText(slidingArrayList.get(position).getTitle());
		}
		return rowView;
	}
}
