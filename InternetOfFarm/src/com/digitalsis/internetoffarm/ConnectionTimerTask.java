package com.digitalsis.internetoffarm;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.TimerTask;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.util.Log;

public class ConnectionTimerTask extends TimerTask{
	
	private static final String TAG = "ConnectionTimerTask";
	private static final int SERVERPORT = 10107;
	private static final String SERVER_IP = "168.126.217.75";
	Socket socket;
	BufferedOutputStream out = null;
	BufferedReader in = null;
	private int seqNum = 0;
	private ConnectionTimerTask connTimerTask;
	protected static ActionListener mRealTimeDataListener;
	
	public void setRealTimeDataListener(ActionListener listener) {
		mRealTimeDataListener = listener;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		//BufferedWriter bufferWriter = null;
		InetAddress serverAddr;
		try {
			serverAddr = InetAddress.getByName(SERVER_IP);
			InetSocketAddress addr = new InetSocketAddress(serverAddr, SERVERPORT);
			socket = new Socket(serverAddr, SERVERPORT);
			if (!socket.isConnected()) {
				socket.connect(addr, 3000);
			}
			socket.setSoTimeout(3000);
			Log.d(TAG, "Connected, seqNum:"+seqNum);
			String str = null;
			if(seqNum < 1){
				str = "GET AllCollect IOFP/1.0\nCseq: "+seqNum+"\n";
			} else {
				str = "GET RealtimeCollect IOFP/1.0\nCseq: "+seqNum+"\n";
			}
			
			byte[] byteStr = str.getBytes("ASCII");

			synchronized (socket) {
				out = new BufferedOutputStream(socket.getOutputStream());
				out.write(byteStr);
				out.flush();
				Log.d(TAG, "Sent");
				in = new BufferedReader(
	                    new InputStreamReader(socket.getInputStream()));
				/*if (-1==in.read() || 0==in.read()) {
					Log.e(TAG, "ConnectionTimerTask return~!    jun02v");
					return;
				}*/
				String revMsg = null;
				XmlFileReceiver xmlReceiver = new XmlFileReceiver();
				while((revMsg = in.readLine().trim()) != null && !revMsg.equals("")) { 
					if (revMsg.startsWith("<AllCollect>")) {
						xmlReceiver.xmlParser(revMsg);
					}
					if (revMsg.startsWith("<RealtimeCollect>")) {
						xmlReceiver.xmlParser(revMsg);
					}
					Log.d(TAG, "jun02v revMsg:"+revMsg);
				}
			}
			Log.d(TAG, "jun02v Realtime data received~!");
			seqNum++;
			
			if (mRealTimeDataListener != null) {
				mRealTimeDataListener.onSuccess();
			}
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
			if (mRealTimeDataListener != null) {
				mRealTimeDataListener.onFailure(0);
			}
		} catch (SocketException e) {
			e.printStackTrace();
			if (mRealTimeDataListener != null) {
				mRealTimeDataListener.onFailure(0);
			}
		} catch (IOException e) {
			e.printStackTrace();
			if (mRealTimeDataListener != null) {
				mRealTimeDataListener.onFailure(0);
			}
		} finally {
			try {
				if (in != null) {
					in.close();
				}
				if (out != null) {
					out.close();
				}
				if (socket != null && !socket.isClosed()) {
					socket.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void setRealTimeData(Socket socket){
		try {
			seqNum++;
			String str = "GET RealtimeCollect IOFP/1.0\nCseq: "+seqNum;
		
			byte[] byteStr;
			byteStr = str.getBytes("ASCII"); 
			//byte[] byteStr2 = str2.getBytes(Charset.defaultCharset());
			out = new BufferedOutputStream(socket.getOutputStream());
			out.write(byteStr);
			out.flush();
			in = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
			String revMsg = null;
			XmlFileReceiver xmlReceiver = new XmlFileReceiver();
			while((revMsg  = in.readLine()) != null) {
				if (revMsg.startsWith("<AllCollect>")) {
					xmlReceiver.xmlParser(revMsg);
				}
				Log.d(TAG, "jun02v : "+revMsg);
			}
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	@Override
	public boolean cancel() {
		// TODO Auto-generated method stub
		seqNum = 0;
		try {
			if (socket != null) {
				socket.close();
			}
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return super.cancel();
	}
	
}
