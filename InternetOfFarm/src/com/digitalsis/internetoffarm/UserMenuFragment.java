package com.digitalsis.internetoffarm;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class UserMenuFragment extends Fragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ListView listView = (ListView) inflater.inflate(R.layout.list, container, false);
		SlidingMenuAdapter adapter = new SlidingMenuAdapter(getActivity(), generateData());
		listView.setAdapter(adapter);
		return listView;
		
	}

	private ArrayList<SlidingMenuItem> generateData() {
		// TODO Auto-generated method stub
		ArrayList<SlidingMenuItem> Items = new ArrayList<SlidingMenuItem>();
		Items.add(new SlidingMenuItem(getActivity().getString(R.string.account)));
		Items.add(new SlidingMenuItem(getActivity().getString(R.string.farm_info)));
		Items.add(new SlidingMenuItem(R.drawable.farm_name, IOFData.getFarmName()));
		Items.add(new SlidingMenuItem(R.drawable.address, IOFData.getFarmAddr()));
		Items.add(new SlidingMenuItem(R.drawable.crops, IOFData.getCropId()));
		return Items;
	}
}
