package com.digitalsis.internetoffarm;

import java.util.Timer;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ArrayAdapter;

import com.actionbarsherlock.view.MenuItem;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.viewpagerindicator.IconPagerAdapter;
import com.viewpagerindicator.TabPageIndicator;

public class IOFActivity extends SlidingFragmentActivity {

	private static final String TAG = "IOFActivity";
	
	private static int iconArray[] = 
			new int[] {R.drawable.status, R.drawable.control, R.drawable.graph};/*, R.drawable.account */
	ArrayAdapter<CharSequence> menuAdapter;	
	StatusFragment statusFragment;
	private BackPressCloseHandler backPressHandler ;

	private boolean isConfigChange = false;

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setTitle(getFarmName());
		setContentView(R.layout.activity_iof);
		com.actionbarsherlock.app.ActionBar actionBar = getSupportActionBar();
		if (findViewById(R.id.menu_frame) == null) {
			setBehindContentView(R.layout.menu_frame);
			getSlidingMenu().setSlidingEnabled(true);
			getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
			// show home as up so we can toggle
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.show();
			
		} else {
			// add a dummy view
			View v = new View(this);
			setBehindContentView(v);
			getSlidingMenu().setSlidingEnabled(false);
			getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
		}
		
		// set the Behind View Fragment
		getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.menu_frame, new UserMenuFragment())
			.commit();
		// customize the SlidingMenu
		SlidingMenu sm = getSlidingMenu();
			sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
			sm.setShadowWidthRes(R.dimen.shadow_width);
			sm.setShadowDrawable(R.drawable.shadow);
			sm.setBehindScrollScale(0.25f);
			sm.setFadeDegree(0.25f);
		
		menuAdapter = ArrayAdapter.createFromResource(this, R.array.menu_title, android.R.layout.simple_list_item_single_choice);
		FragmentPagerAdapter adapter = new IOFAdapter(getSupportFragmentManager());
		
		TabPageIndicator indicator = (TabPageIndicator)findViewById(R.id.indicator);
		
		ViewPager pager = (ViewPager)findViewById(R.id.pager);
		pager.setAdapter(adapter);
		indicator.setViewPager(pager);
		statusFragment = StatusFragment.getInstance(this);
		
		backPressHandler = new BackPressCloseHandler(this);
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			toggle();
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		backPressHandler.onBackPressed();
	}
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Intent disconnIntent = new Intent();
		//connIntent.setAction("com.digitalsis.internetoffarm.CONNECTION_SERVICE");
		disconnIntent.setComponent(FirstActivity.mService);
		stopService(disconnIntent);
		Log.e(TAG, "service cancle~!");
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}
	
	public class IOFAdapter extends FragmentPagerAdapter implements
	IconPagerAdapter {
		
		public IOFAdapter(FragmentManager fm) {
			super(fm);
		}
		
		@Override
		public int getIconResId(int index) {
			// TODO Auto-generated method stub
			return iconArray[index];
		}
		
		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			int pos = position % menuAdapter.getCount();
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			switch(pos) {
				case 0 :
					return statusFragment;
				case 1 :
					return ControlFragment.getInstance();
				case 2 :
					return GraphFragment.getInstance();
				/*case 3 :
					return AccountFragment.getInstance();*/
				default :
					return null;
			}
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return menuAdapter.getCount();
		}
		
		@Override
		public CharSequence getPageTitle(int position) {
			// TODO Auto-generated method stub
			return menuAdapter.getItem(position % menuAdapter.getCount());
		}
	}
}