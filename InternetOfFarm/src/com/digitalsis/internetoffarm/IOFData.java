package com.digitalsis.internetoffarm;

import android.util.Log;

public class IOFData {

	private static final String TAG = "IOFData";
	public static final String ALL_COL = "AllCollect";
	public static final String INITIAL_COL = "InitialCollect";
	public static final String FARM_STATUS = "FarmStatus";
	public static final String FARM_NAME = "FarmName";
	public static final String FARM_ADDR = "FarmAddr";
	public static final String FARM_START_YEAR = "FarmStartYear";
	public static final String MULTI_CONTL_MODEL = "MultiConrollerModel";
	public static final String CROP_INFO = "CropInfo";
	public static final String CROP_ID = "CorpID";
	public static final String CROP_TYPE = "CorpType";
	public static final String GROW_START = "GrowthStartDate";
	public static final String GROW_END = "GrowthEndDate";
	public static final String GROW_ENV_SET = "GrowthEnvironmentSet";
	public static final String TEMP = "Temp";
	public static final String HUMIDITY = "Humidity";
	public static final String SET_PPFD = "PPFD";
	public static final String CO2 = "CO2";
	public static final String REALTIME = "RealtimeCollect";
	public static final String INDOOR_ENV = "IndoorEnvironment";
	public static final String PPFD = "Ppfd";
	public static final String WATER_LEV = "WaterLevel";
	public static final String FLOOD = "Flood";
	public static final String DO = "DO";
	public static final String PH = "pH";
	public static final String WATER_TEMP = "WaterTemp";
	public static final String EC = "EC";
	public static final String ELEC_SENSOR = "ElectricPowerSensor";
	public static final String ELEC_STATUS = "ElectricPowerStatus";
	public static final String OUTDOOR_ENV = "OutdoorEnvironment";
	
	public static final String BLIND_CONTROL = "BlindControl";
	public static final String CO2_CONTROL = "Co2Control";
	public static final String FAN_CONTROL = "FanControl";
	public static final String HEATER_CONTROL = "HeaterControl";
	public static final String LED_CONTROL = "LedControl";
	
	public static String farmName;
	public static String farmAddr;
	public static String cropId;
	public static String cropType;
	
	public static String tempIn = "-2000";
	public static String humidityIn;
	public static String ppfdIn;
	public static String waterLevelIn;
	public static String floodIn;
	public static String inDOIn;
	public static String phIn;
	public static String waterTempIn;
	public static String ecIn;
	public static String electricSensorIn;
	public static String electricStatusIn;
	public static String co2In;
	
	public static String tempOut;
	public static String humidityOut;
	public static String ppfdOut;
	public static String windSpeedOut;
	public static String windDirectionOut;
	public static String rainFallOut;
	
	public static int blindControl;
	public static int co2Control;
	public static int fanControl;
	public static int heaterControl;
	public static int ledControl;
	
	public static String getTempIn() {
		return tempIn;
	}
	public static void setTempIn(String tempIn) {
		Log.d(TAG, "set tempIn : "+tempIn);
		IOFData.tempIn = tempIn;
	}
	public static String getHumidityIn() {
		return humidityIn;
	}
	public static void setHumidityIn(String humidityIn) {
		Log.d(TAG, "set humidityIn : "+humidityIn);
		IOFData.humidityIn = humidityIn;
	}
	public static String getPpfdIn() {
		return ppfdIn;
	}
	public static void setPpfdIn(String ppfdIn) {
		Log.d(TAG, "set ppfdIn : "+ppfdIn);
		IOFData.ppfdIn = ppfdIn;
	}
	public static String getWaterLevelIn() {
		return waterLevelIn;
	}
	public static void setWaterLevelIn(String waterLevelIn) {
		Log.d(TAG, "set waterLevelIn : "+waterLevelIn);
		IOFData.waterLevelIn = waterLevelIn;
	}
	public static String getFloodIn() {
		return floodIn;
	}
	public static void setFloodIn(String floodIn) {
		Log.d(TAG, "set floodIn : "+floodIn);
		IOFData.floodIn = floodIn;
	}
	public static String getInDOIn() {
		return inDOIn;
	}
	public static void setInDOIn(String inDOIn) {
		Log.d(TAG, "set inDOIn : "+inDOIn);
		IOFData.inDOIn = inDOIn;
	}
	public static String getPhIn() {
		return phIn;
	}
	public static void setPhIn(String phIn) {
		Log.d(TAG, "set phIn : "+phIn);
		IOFData.phIn = phIn;
	}
	public static String getWaterTempIn() {
		return waterTempIn;
	}
	public static void setWaterTempIn(String waterTempIn) {
		Log.d(TAG, "set waterTempIn : "+waterTempIn);
		IOFData.waterTempIn = waterTempIn;
	}
	public static String getEcIn() {
		return ecIn;
	}
	public static void setEcIn(String ecIn) {
		Log.d(TAG, "set ecIn : "+ecIn);
		IOFData.ecIn = ecIn;
	}
	public static String getElectricSensorIn() {
		return electricSensorIn;
	}
	public static void setElectricSensorIn(String electricSensorIn) {
		Log.d(TAG, "set electricSensorIn : "+electricSensorIn);
		IOFData.electricSensorIn = electricSensorIn;
	}
	public static String getElectricStatusIn() {
		return electricStatusIn;
	}
	public static void setElectricStatusIn(String electricStatusIn) {
		Log.d(TAG, "set electricStatusIn : "+electricStatusIn);
		IOFData.electricStatusIn = electricStatusIn;
	}
	public static String getCo2In() {
		return co2In;
	}
	public static void setCo2In(String co2In) {
		Log.d(TAG, "set co2In : "+co2In);
		IOFData.co2In = co2In;
	}
	public static String getTempOut() {
		return tempOut;
	}
	public static void setTempOut(String tempOut) {
		Log.d(TAG, "set tempOut : "+tempOut);
		IOFData.tempOut = tempOut;
	}
	public static String getHumidityOut() {
		return humidityOut;
	}
	public static void setHumidityOut(String humidityOut) {
		Log.d(TAG, "set humidityOut : "+humidityOut);
		IOFData.humidityOut = humidityOut;
	}
	public static String getPpfdOut() {
		return ppfdOut;
	}
	public static void setPpfdOut(String ppfdOut) {
		Log.d(TAG, "set ppfdOut : "+ppfdOut);
		IOFData.ppfdOut = ppfdOut;
	}
	public static String getWindSpeedOut() {
		return windSpeedOut;
	}
	public static void setWindSpeedOut(String windSpeedOut) {
		Log.d(TAG, "set windSpeedOut : "+windSpeedOut);
		IOFData.windSpeedOut = windSpeedOut;
	}
	public static String getWindDirectionOut() {
		return windDirectionOut;
	}
	public static void setWindDirectionOut(String windDirectionOut) {
		Log.d(TAG, "set windDirectionOut : "+windDirectionOut);
		IOFData.windDirectionOut = windDirectionOut;
	}
	public static String getRainFallOut() {
		return rainFallOut;
	}
	public static void setRainFallOut(String rainFallOut) {
		Log.d(TAG, "set rainFallOut : "+rainFallOut);
		IOFData.rainFallOut = rainFallOut;
	}
	public static String getFarmName() {
		return farmName;
	}
	public static void setFarmName(String farmName) {
		IOFData.farmName = farmName;
	}
	public static String getFarmAddr() {
		return farmAddr;
	}
	public static void setFarmAddr(String farmAddr) {
		IOFData.farmAddr = farmAddr;
	}
	public static String getCropId() {
		return cropId;
	}
	public static void setCropId(String cropId) {
		IOFData.cropId = cropId;
	}
	public static String getCropType() {
		return cropType;
	}
	public static void setCropType(String cropType) {
		IOFData.cropType = cropType;
	}
	
	public static int getBlindControl() {
		return blindControl;
	}
	public static void setBlindControl(String blindControl) {
		IOFData.blindControl = blindControl.equals("0") ? 0 : 1;
	}
	public static int getCo2Control() {
		return co2Control;
	}
	public static void setCo2Control(String co2Control) {
		IOFData.co2Control = co2Control.equals("0") ? 0 : 1;
	}
	public static int getFanControl() {
		return fanControl;
	}
	public static void setFanControl(String fanControl) {
		IOFData.fanControl = fanControl.equals("0") ? 0 : 1;
	}
	public static int getHeaterControl() {
		return heaterControl;
	}
	public static void setHeaterControl(String heaterControl) {
		IOFData.heaterControl = heaterControl.equals("0") ? 0 : 1;
	}
	public static int getLedControl() {
		return ledControl;
	}
	public static void setLedControl(String ledControl) {
		IOFData.ledControl = Integer.parseInt(ledControl);
	}

}
