package com.digitalsis.internetoffarm;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Timer;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class ConnectionService extends Service {

	private static final String TAG = "ConnectionService";
	private static final int SERVERPORT = 10107;
	private static final String SERVER_IP = "168.126.217.75";
	Socket socket;
	BufferedOutputStream out = null;
	BufferedReader in = null;
	private int seqNum = 0;
	private Timer timer = new Timer();
	private ConnectionTimerTask connTimerTask;
	SocketConnectionThread socketThread;
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Log.d(TAG, "onCreate()");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.d(TAG, "service start~!");
		connTimerTask = new ConnectionTimerTask();
		timer.schedule(connTimerTask, 0, 5000);
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		//connTimerTask.cancel();
		super.onDestroy();
		Log.d(TAG, "onDestroy()");
		seqNum = 0;
		try {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (timer != null) {
			timer.cancel();
		}
		if (connTimerTask != null) {
			connTimerTask.cancel();
		}
		if (socketThread != null) {
			socketThread.setConnectionClose();
		}
		
	}

}
