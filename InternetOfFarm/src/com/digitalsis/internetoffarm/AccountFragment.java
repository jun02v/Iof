package com.digitalsis.internetoffarm;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class AccountFragment extends Fragment {
	
	private static AccountFragment aFragment = null;

	public static Fragment getInstance() {
		// TODO Auto-generated method stub
		if(aFragment == null) {
			aFragment = new AccountFragment();
		}		
		return aFragment;
	}
	
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		LinearLayout accountLayout = (LinearLayout)inflater.inflate(R.layout.fragment_account, container, false);
		
		return accountLayout;
	}

}
