package com.digitalsis.internetoffarm;

import java.util.Timer;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class FirstActivity extends Activity implements View.OnClickListener {

	private static final String TAG = "FirstActivity";
	XmlFileReceiver fileReceiver;
	Thread connThread;
	TextView enter;
	ProgressBar progressConn;
	private volatile SocketConnectionThread socketThread = null;
	public static ComponentName mService;
	public static final int CONN_SUCCESS = 1, CONN_FAIL = 2; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first);
		enter = (TextView)findViewById(R.id.enter_in);
		enter.setOnClickListener(this);
		progressConn = (ProgressBar)findViewById(R.id.progress_connection);
		fileReceiver = XmlFileReceiver.getInstance();
		fileReceiver.getFile();
		socketThread = new SocketConnectionThread();
		socketThread.setSocketConnectionListener(new ActionListener() {
			
			@Override
			public void onSuccess() {
				// TODO Auto-generated method stub
				Intent connIntent = new Intent();
				connIntent.setAction("com.digitalsis.internetoffarm.CONNECTION_SERVICE");
				mService = startService(connIntent);
				Log.d(TAG, "connectionListener success, service start~!!!");
				mHandler.sendEmptyMessageDelayed(CONN_SUCCESS, 4000);
			}
			
			@Override
			public void onFailure(int reason) {
				// TODO Auto-generated method stub
				Log.d(TAG, "connectionListener FAIL~!");
				mHandler.sendEmptyMessageDelayed(CONN_FAIL, 10);
			}
		});
	}
	
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch(msg.what){
			case CONN_SUCCESS:
				Toast.makeText(getApplication(), R.string.connect_server, Toast.LENGTH_LONG);
				Intent intent = new Intent(FirstActivity.this, IOFActivity.class);
				startActivity(intent);
				Log.e(TAG, "Handler Call IOFActivity~!!!");
				finish();
				break;
			case CONN_FAIL:
				Toast.makeText(getApplication(), R.string.cannot_connect_server, Toast.LENGTH_LONG);
				Log.e(TAG, "Handler FINISH!!!");
				finish();
				break;
			}
		}
	};

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		progressConn.setVisibility(View.VISIBLE);
		enter.setVisibility(View.GONE);
		if (!socketThread.isAlive()) {
			socketThread.start();
		}	
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		socketThread.interrupt();
		//timer.cancel();
	}
	
}
