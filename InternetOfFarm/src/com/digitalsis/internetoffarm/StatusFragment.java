package com.digitalsis.internetoffarm;

//import android.app.Fragment;
import java.util.Timer;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class StatusFragment extends Fragment {
	
	private static final String TAG = "StatusFragment";
	private static StatusFragment sFragment = null;
	private LinearLayout ppfdLayout;
	private LinearLayout tempLayout;
	public TextView tempText, humidityText, ppfdText, co2Text, ecText, phText, doText, waterLevelText, waterTempText,
		floodText, leafTempText, stemChangeText, weightText, windDirectText, windSpeedText, rainfallText, ledText, 
		blind1Text, blind2Text, heaterText, wateringText, powerSensorText, powerStatusText;
	private Messenger mService;
	private final Messenger mMessenger = new Messenger(new IncommingHandler());
	
	private Timer timer = new Timer();
	private ConnectionTimerTask connTimerTask = new ConnectionTimerTask();
	private static Activity mMainActivity;
	
	private static final int MSG_REGISTER_CLIENT = 1;
	private static final int MSG_SET_VALUE = 2;
	private static final int MSG_UNREGISTER_CLIENT = 3; 
	private static final int MSG_READ_REALTIME_DATA = 4;
	
	private boolean isConfigChange = false;
	
	public static StatusFragment getInstance(Activity activity){
		if(sFragment == null){
			sFragment = new StatusFragment();
		}
		mMainActivity = activity;
		return sFragment;
	}
	
	private void setListener() {
		connTimerTask.setRealTimeDataListener(new ActionListener() {
			
			@Override
			public void onSuccess() {
				// TODO Auto-generated method stub
				mHandler.sendEmptyMessageDelayed(MSG_READ_REALTIME_DATA, 100);
				Log.d(TAG, "Real time data read success~~!! ");
			}
			
			@Override
			public void onFailure(int reason) {
				// TODO Auto-generated method stub
				Log.e(TAG, "Real time data read fail~!");
			}
		});
	}
	
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch(msg.what){
			case MSG_READ_REALTIME_DATA :
				if (isAdded()) {
					setStatusData();
				}
				break;
			}
		}
	};
	
	class IncommingHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			switch (msg.what) {
			case MSG_SET_VALUE :
				Log.d(TAG, msg.arg1+"");
				break;
			default :
				super.handleMessage(msg);
			}
		}
	}
	
	ServiceConnection conn = new ServiceConnection() {
		
		@Override
		public void onServiceDisconnected(ComponentName name) {
			// TODO Auto-generated method stub
			Log.d(TAG, "onServiceDisconnected.");
		}
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// TODO Auto-generated method stub
			mService = new Messenger(service);
			Log.d(TAG, "onServiceConnected()");
			try {
				Message msg = Message.obtain(null, MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);
				
				//msg = Message.obtain(h, what, obj)
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//connTimerTask = new ConnectionTimerTask();
		//connTimerTask.cancel();
		//timer.schedule(connTimerTask, 5000, 5000);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		//return super.onCreateView(inflater, container, savedInstanceState);
		LinearLayout statusLayout = (LinearLayout)inflater.inflate(R.layout.fragment_stauts, container, false);
		//tempText = (TextView)inflater.inflate(R.id.temperature_text, container);
		tempText = (TextView) statusLayout.findViewById(R.id.temperature_text);
		humidityText = (TextView) statusLayout.findViewById(R.id.humidity_text);
		ppfdText = (TextView) statusLayout.findViewById(R.id.ppfd_text);
		co2Text = (TextView) statusLayout.findViewById(R.id.co2_text);
		ppfdLayout = (LinearLayout) statusLayout.findViewById(R.id.ppfd_layout);
		tempLayout = (LinearLayout) statusLayout.findViewById(R.id.temperature_layout);
		ecText = (TextView) statusLayout.findViewById(R.id.ec_value);
		phText = (TextView) statusLayout.findViewById(R.id.ph_value);
		doText = (TextView) statusLayout.findViewById(R.id.do_value);
		waterLevelText = (TextView) statusLayout.findViewById(R.id.water_level_value);
		waterTempText = (TextView) statusLayout.findViewById(R.id.water_temp_value);
		floodText = (TextView) statusLayout.findViewById(R.id.flood_value);
		setStatusData();
		setListener();
		return statusLayout;
	}
	
	public void setStatusData() {
		tempText.setText(IOFData.getTempIn()+getString(R.string.temp_character));
		humidityText.setText(IOFData.getHumidityIn()+"%");
		ppfdText.setText(IOFData.getPpfdIn()+"");
		co2Text.setText(IOFData.getCo2In()+"ppm");
		DisplayMetrics outMetrics = new DisplayMetrics();
		mMainActivity.getWindowManager().getDefaultDisplay().getMetrics(outMetrics);
		if (IOFData.getCo2In() != null && IOFData.getCo2In().length() > 3) {
			co2Text.setTextSize(23);
			tempLayout.setPadding((int)(15*outMetrics.density), (int)(30*outMetrics.density),
					(int)(30*outMetrics.density), (int)(30*outMetrics.density));
			ppfdLayout.setPadding((int)(30*outMetrics.density), (int)(30*outMetrics.density),
					(int)(25*outMetrics.density), (int)(30*outMetrics.density));
		}
		
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		timer.cancel();
		Log.e(TAG, "timer cancle~!");
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}
}
