package com.digitalsis.internetoffarm;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.Toast;

public class ControlFragment extends Fragment implements SeekBar.OnSeekBarChangeListener, OnTouchListener {
	
	private static final String TAG = "ControlFragment";
	private static ControlFragment cFragment = null;
	private static final int SERVERPORT = 10107;
	private static final String SERVER_IP = "168.126.217.75";
	private static final String DLD = "DLD", DVE = "DVE", DHT = "DHT", DWS = "DWS", DCS = "DCS", DBL = "DBL";
	private static final int DLD_TYPE = 21, DVE_TYPE = 22, DHT_TYPE = 24, DWS_TYPE = 25, DCS_TYPE = 26, DBL_TYPEL = 23;
	private int progressVal;
	private int ledOriginVal;
	public LinearLayout controlLayout;
	public SeekBar ledSeekBar;
	private static int seqNum = 0;
	private BufferedOutputStream out;
	private BufferedReader in;
	Socket socket;
	Thread connThread;
	Switch fanManualSwitch;
	Switch heaterManualSwitch;
	Switch waterManualSwitch;
	ProgressBar ledProgressBar;
	ProgressBar fanProgressBar;
	ProgressBar boilerProgressBar;
	ProgressBar wateringProgressBar;
	
	public static ControlFragment getInstance(){
		if(cFragment == null){
			cFragment = new ControlFragment();
		}
		return cFragment;
	}
	
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Log.d(TAG, "onCreate");
		super.onCreate(savedInstanceState);
		connThread = new Thread(threadRun);
		connThread.start();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Log.d(TAG, "onCreateView");
		controlLayout = (LinearLayout) inflater.inflate(R.layout.fragment_control, container, false);
		Switch ledSwitch = (Switch) controlLayout.findViewById(R.id.led_switch);
		Switch fanSwitch = (Switch) controlLayout.findViewById(R.id.fan_switch);
		Switch blind1Switch = (Switch) controlLayout.findViewById(R.id.blind1_switch);
		Switch blind2Switch = (Switch) controlLayout.findViewById(R.id.blind2_switch);
		Switch heaterSwitch = (Switch) controlLayout.findViewById(R.id.boiler_switch);
		Switch waterSwitch = (Switch) controlLayout.findViewById(R.id.watering_switch);
		
		ledSeekBar  = (SeekBar) controlLayout.findViewById(R.id.led_seekBar);
		fanManualSwitch = (Switch) controlLayout.findViewById(R.id.fan_manual_switch);
		heaterManualSwitch = (Switch) controlLayout.findViewById(R.id.boiler_manual_switch);
		waterManualSwitch = (Switch) controlLayout.findViewById(R.id.watering_manual_switch);
		
		ledProgressBar = (ProgressBar) controlLayout.findViewById(R.id.led_progress);
		fanProgressBar = (ProgressBar) controlLayout.findViewById(R.id.fan_progress);
		boilerProgressBar = (ProgressBar) controlLayout.findViewById(R.id.boiler_progress);
		wateringProgressBar = (ProgressBar) controlLayout.findViewById(R.id.watering_progress);
		
		ledSeekBar.setMax(6);
		ledSeekBar.incrementProgressBy(1);
		ledSeekBar.setOnSeekBarChangeListener(this);
		
		setValues();
		
		//fanManualSwitch.setOnCheckedChangeListener(this);
		fanManualSwitch.setOnTouchListener(this);
		fanManualSwitch.setTag(DVE_TYPE);
		//heaterManualSwitch.setOnCheckedChangeListener(this);
		heaterManualSwitch.setOnTouchListener(this);
		heaterManualSwitch.setTag(DHT_TYPE);
		//waterManualSwitch.setOnCheckedChangeListener(this);
		waterManualSwitch.setOnTouchListener(this);
		waterManualSwitch.setTag(DWS_TYPE);
		
		return controlLayout;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if (event.getAction() == MotionEvent.ACTION_UP) {
			switch(v.getId()) {
			case R.id.fan_manual_switch :
				Log.d(TAG, "check fanManualSwitch~!");
				ControlAsyncTask ventAsyncTask = new ControlAsyncTask();
				ventAsyncTask.setProgressBar(fanProgressBar);
				ventAsyncTask.setSwitch(fanManualSwitch, getActivity());
				ventAsyncTask.setValues(DVE_TYPE, fanManualSwitch.isChecked() ? 0 : 1);
				ventAsyncTask.execute();
				break;
			case R.id.boiler_manual_switch :
				Log.d(TAG, "check heaterManualSwitch~!");
				ControlAsyncTask heaterAsyncTask = new ControlAsyncTask();
				heaterAsyncTask.setProgressBar(boilerProgressBar);
				heaterAsyncTask.setSwitch(heaterManualSwitch, getActivity());
				heaterAsyncTask.setValues(DHT_TYPE, heaterManualSwitch.isChecked() ? 0 : 1);
				heaterAsyncTask.execute();
				break;
			case R.id.watering_manual_switch :
				Log.d(TAG, "check waterManualSwitch~!");
				ControlAsyncTask waterAsyncTask = new ControlAsyncTask();
				waterAsyncTask.setProgressBar(wateringProgressBar);
				waterAsyncTask.setSwitch(waterManualSwitch, getActivity());
				waterAsyncTask.setValues(DWS_TYPE, heaterManualSwitch.isChecked() ? 0 : 1);
				waterAsyncTask.execute();
				//sendControlValue(DWS, DWS_TYPE, isChecked ? 0 : 1);
				break;
			}
		}
		return true;
	}
	
	private void setValues() {
		ledSeekBar.setProgress(IOFData.getLedControl());
		ledOriginVal = IOFData.getLedControl();
		fanManualSwitch.setChecked((IOFData.getFanControl() == 0) ? false : true);
		heaterManualSwitch.setChecked((IOFData.getHeaterControl() == 0) ? false : true);
	}
	
	Runnable threadRun = new Runnable() {
		public void run() {
			// TODO Auto-generated method stub
			//BufferedWriter bufferWriter = null;
			InetAddress serverAddr;
			try {
				Thread.sleep(1000);
				serverAddr = InetAddress.getByName(SERVER_IP);
				InetSocketAddress addr = new InetSocketAddress(serverAddr, SERVERPORT);
				socket = new Socket(serverAddr, SERVERPORT);
				if (!socket.isConnected()) {
					socket.connect(addr, 3000);
					socket.setSoTimeout(3000);
				}
				Log.d(TAG, "connecting success!!!");
				
				String str = "GET EnvironmentControl IOFP/1.0\nCseq: "+seqNum+"\n";
				byte[] byteStr = str.getBytes("ASCII");
				
				synchronized (socket) {
					out = new BufferedOutputStream(socket.getOutputStream());
					out.write(byteStr);
					out.flush();
					Log.d(TAG, "Sent");

					in = new BufferedReader(
		                    new InputStreamReader(socket.getInputStream()));
					if (-1==in.read() || 0==in.read()) {
						Log.e(TAG, "ConnectionService return~!    jun02v");
						return;
					}
					String revMsg = null;
					XmlFileReceiver xmlReceiver = new XmlFileReceiver();
					while((revMsg=in.readLine().trim())!=null && !revMsg.equals("")) { 
						Log.d(TAG, "jun02v : "+revMsg);
						if (revMsg.startsWith("<EnvironmentControl>")) {
							xmlReceiver.xmlParser(revMsg);
						}
					}
					getActivity().runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							setValues();
						}
					});
				}
			} catch (UnknownHostException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				try {
					if (in != null) {
						in.close();
					}
					if (out != null) {
						out.close();
					}
					if (socket != null && !socket.isClosed()) {
						socket.close();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	};

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub
		progressVal = progress;
		Log.e(TAG, "onProgressChanged");
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		Log.e(TAG, "onStartTrackingTouch");
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		Log.e(TAG, "onStopTrackingTouch");
		ControlAsyncTask ledAsyncTask = new ControlAsyncTask();
		ledAsyncTask.setProgressBar(ledProgressBar);
		ledAsyncTask.setSeekBar(ledSeekBar, getActivity());
		ledAsyncTask.setValues(DLD_TYPE, progressVal, ledOriginVal);
		ledAsyncTask.execute();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		seqNum = 0;
		try {
			if (socket != null && !socket.isClosed()) {
				socket.close();
			}
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}
}
