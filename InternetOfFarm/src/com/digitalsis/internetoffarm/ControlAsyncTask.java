package com.digitalsis.internetoffarm;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.Toast;

public class ControlAsyncTask extends AsyncTask<Void, Integer, Integer> {
	
	private static final String TAG = "ControlAsyncTask";
	private static final int SERVERPORT = 10107;
	private static final String SERVER_IP = "168.126.217.75";
	private static final String DLD = "DLD", DVE = "DVE", DHT = "DHT", DWS = "DWS", DCS = "DCS", DBL = "DBL";
	private static final int DLD_TYPE = 21, DVE_TYPE = 22, DHT_TYPE = 24, DWS_TYPE = 25, DCS_TYPE = 26, DBL_TYPEL = 23;
	private static final int OK_200 = 0, NOT_ACCEPT_406 = 1, NO_RESPONSE = 2, DEFAULT = 10;
	private WeakReference<Switch> mSwitchWeakReference;
	private WeakReference<ProgressBar> mProgressBarWeakReference;
	private WeakReference<SeekBar> mSeekBarWeakReference;
	private Socket socket;
	private BufferedOutputStream out;
	private BufferedReader in;
	private String mActuator;
	private int mType, mValue, ledOriginValue;
	private static int seqNum = 0;
	private int mResult;
	private Context mContext;

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		mProgressBarWeakReference.get().setVisibility(View.VISIBLE);
		if (mSwitchWeakReference != null) {
			mSwitchWeakReference.get().setEnabled(false);
		}
		if (mSeekBarWeakReference != null) {
			mSeekBarWeakReference.get().setEnabled(false);
		}
	}
	
	@Override
	protected Integer doInBackground(Void... params) {
		// TODO Auto-generated method stub
		InetAddress serverAddr;
		try {
			serverAddr = InetAddress.getByName(SERVER_IP);
			InetSocketAddress addr = new InetSocketAddress(serverAddr, SERVERPORT);
			socket = new Socket(serverAddr, SERVERPORT);
			if (!socket.isConnected()) {
				socket.connect(addr, 3000);
				socket.setSoTimeout(3000);
			}
			Log.d(TAG, "Connected");
			String str = "SET Control IOFP/1.0\nCseq: "+seqNum+"\nLength: 32\n<Control><"+mActuator+">"+mType+", "+mValue+"</"+mActuator+"></Control>\n";
			byte[] byteStr = str.getBytes("ASCII");
			synchronized (socket) {
				//BufferedWriter sendBufferWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
				Log.d(TAG, "send str:"+str);
				out = new BufferedOutputStream(socket.getOutputStream());
				out.write(byteStr);
				out.flush();
				Log.d(TAG, "Sent");
				
				in = new BufferedReader(
	                    new InputStreamReader(socket.getInputStream()));
				if (-1==in.read() || 0==in.read()) {
					Log.e(TAG, "sendThread return~!    jun02v");
					return NO_RESPONSE;
				}
				String revMsg = null;
				mResult = DEFAULT;
				while((revMsg=in.readLine().trim())!=null && !revMsg.equals("")) { 
					Log.d(TAG, "revMsg : "+revMsg);
					if ("200 OK".equals(revMsg)) {
						mResult = OK_200;
						Log.d(TAG, "200 OK!, result false~!!");
					}
					if (revMsg.contains("406 Not Acceptable")) {
						mResult = NOT_ACCEPT_406;
						Log.d(TAG, "406 Not Acceptable~!!!, result false~!!");
					}
				}
			}
			seqNum++;
		} catch(UnknownHostException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
				if (out != null) {
					out.close();
				}
				if (socket != null && !socket.isClosed()) {
					socket.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return mResult;
	}

	@Override
	protected void onPostExecute(Integer result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		Log.d(TAG, "onPostExecute, result:"+result);
		switch (result) {
		case OK_200 :
			Toast.makeText(mContext, "SUCCESS!", Toast.LENGTH_SHORT).show();
			break;
		case NOT_ACCEPT_406 :
			Log.d(TAG, "onPostExecute, NOT_ACCEPT_406");
			Toast.makeText(mContext, "406 Not Acceptable.", Toast.LENGTH_SHORT).show();
			break;
		case NO_RESPONSE :
			Toast.makeText(mContext, "No Responsible.", Toast.LENGTH_SHORT).show();
			break;
		case DEFAULT :
			Toast.makeText(mContext, "FAIL.", Toast.LENGTH_SHORT).show();
			break;
		} 
		if(result != OK_200){ // fail
			if (mSwitchWeakReference != null) {
				mSwitchWeakReference.get().setChecked(mValue == 0 ? true : false);
				Log.d(TAG, "setChecked, mValue:"+mValue);
			}
			if (mSeekBarWeakReference != null) {
				Log.d(TAG, "led value back~!!");
				mSeekBarWeakReference.get().setProgress(ledOriginValue);
			}
		}
		mProgressBarWeakReference.get().setVisibility(View.GONE);
		if (mSwitchWeakReference != null) {
			mSwitchWeakReference.get().setEnabled(true);
		}
		if (mSeekBarWeakReference != null) {
			mSeekBarWeakReference.get().setEnabled(true);
		}
	}

	@Override
	protected void onCancelled() {
		// TODO Auto-generated method stub
		super.onCancelled();
	}
	
	protected void setSwitch(Switch crtlSwitch, Context context) {
		mSwitchWeakReference = new WeakReference<Switch>(crtlSwitch);
		mContext = context;
		setActuator();
		
	}
	
	protected void setSeekBar(SeekBar crtlSeekBar, Context context) {
		mSeekBarWeakReference = new WeakReference<SeekBar>(crtlSeekBar);
		mContext = context;
		setActuator();
	}
	
	protected void setProgressBar(ProgressBar progressBar) {
		mProgressBarWeakReference = new WeakReference<ProgressBar>(progressBar);
	}
	
	protected void setActuator() {
		if (mSeekBarWeakReference != null) {
			if (mSeekBarWeakReference.get().getId() == R.id.led_seekBar) {
				mActuator = DLD;
			}
		}
		if (mSwitchWeakReference != null) {
			switch (mSwitchWeakReference.get().getId()) {
			case R.id.fan_manual_switch :
				mActuator = DVE;
				break;
			case R.id.boiler_manual_switch :
				mActuator = DHT;
				break;
			case R.id.watering_manual_switch :
				mActuator = DWS;
				break;
			}
		}
		Log.d(TAG, "setActuator:"+mActuator);
	}
	
	protected void setValues(int type, int value) {
		mType = type;
		mValue = value;
	}
	
	protected void setValues(int type, int value, int ledOrgVal) {
		mType = type;
		mValue = value;
		ledOriginValue = ledOrgVal;
	}
}
