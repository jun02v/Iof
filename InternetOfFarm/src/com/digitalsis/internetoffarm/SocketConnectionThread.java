package com.digitalsis.internetoffarm;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.util.Log;

public class SocketConnectionThread extends Thread {
	
	private static final String TAG = "SocketConnectionThread";
	private static final int SERVERPORT = 10107;
	private static final String SERVER_IP = "168.126.217.75";
	private Socket socket;
	private boolean isTryConnction = true;
	private int numOfConnection = 0;
	private SocketConnectionThread socketThread = null;
	protected static ActionListener mSocketConnectionListener;
	
	public void setSocketConnectionListener(ActionListener listener) {
		mSocketConnectionListener = listener;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(isTryConnction) {
			Log.d(TAG, "Thread entered, connecting...");
			InetAddress serverAddr;
			try {
				serverAddr = InetAddress.getByName(SERVER_IP);
				InetSocketAddress addr = new InetSocketAddress(serverAddr, SERVERPORT);
				socket = new Socket(serverAddr, SERVERPORT);
				if (!socket.isConnected()) {
					socket.connect(addr, 3000);
				}
				socket.setSoTimeout(3000);
				Log.d(TAG, "connecting success!!!");
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				isTryConnction = false;
				mSocketConnectionListener.onFailure(0);
				Log.d(TAG, "connection fail, return~!");
				e.printStackTrace();
			} finally {
				try {
					if (socket != null && !socket.isClosed()) {
						Log.e(TAG, "finally socket closed!!!");
						socket.close();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (socket != null) {
				if (socket.isConnected()) {
					isTryConnction = false;
					mSocketConnectionListener.onSuccess();
					Log.d(TAG, "connection success~!!");
				} else {
					try {
						Thread.sleep(1000);
						numOfConnection++;
						Log.d(TAG, "connection fail, numOfConnection:"+numOfConnection);
						if (numOfConnection > 3) {
							isTryConnction = false;
							mSocketConnectionListener.onFailure(0);
							Log.d(TAG, "connection fail, return~!");
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				return;
			}
		}
		super.run();
	}
	
	public boolean isSocketConnected() {
		if (socket != null) {
			return socket.isConnected();
		} else {
			return false;
		}
	}
	
	public Socket getSocket() {
		return socket;
	}
	
	public void setConnectionClose() {
		try {
			if (socket != null && !socket.isClosed()) {
				socket.close();
			} else {
				Log.d(TAG, "Socket is null Or closed~!");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void interrupt() {
		// TODO Auto-generated method stub
		Log.e(TAG, "SocketConnectionThread is interrupted~!");
		super.interrupt();
	}
}
